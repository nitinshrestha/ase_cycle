﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cycle
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }


        private void Form3_Load(object sender, EventArgs e)
        {
            int[] HR;
            int[] SPD;
            int[] CAD;
            int[] ALT;
            int[] PWR;


            // Datagrid view colum header
            dataGridView1.Columns.Add("SN", "SN");
            dataGridView1.Columns.Add("HR", "HR");
            dataGridView1.Columns.Add("SPD", "SPD");
            dataGridView1.Columns.Add("CAD", "CAD");
            dataGridView1.Columns.Add("ALT", "ALT");
            dataGridView1.Columns.Add("PWR", "PWR");

            try
            {
                string txtData = File.ReadAllText("ASDBExampleCycleComputerData.hrm");
                string[] arrData = txtData.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                int index2 = Array.IndexOf(arrData, "[HRData]");
                HR = new int[arrData.Length - (index2 + 1)];
                SPD = new int[arrData.Length - (index2 + 1)];
                CAD = new int[arrData.Length - (index2 + 1)];
                ALT = new int[arrData.Length - (index2 + 1)];
                PWR = new int[arrData.Length - (index2 + 1)];

                int j = 0;
                double total = 0;
                double heart = 0;
                double powertotal = 0;
                double alt = 0;
                double sn = 1;


                for (int i = index2 + 1; i < arrData.Length; i++)
                {
                    string HRData = arrData[i];
                    string[] arrHrdata = Regex.Split(HRData, @"\W+");

                    HR[j] = Convert.ToInt32(arrHrdata[0]);
                    SPD[j] = Convert.ToInt32(arrHrdata[1]);
                    CAD[j] = Convert.ToInt32(arrHrdata[2]);
                    ALT[j] = Convert.ToInt32(arrHrdata[3]);
                    PWR[j] = Convert.ToInt32(arrHrdata[4]);


                    // for Average speed
                    double speed = Convert.ToDouble(arrHrdata[1]) / 10;
                    double xa = SPD[j];
                    total = total + xa;

                    //Average heart Rate
                    double hrt = HR[j];
                    heart = heart + hrt;


                    //Average power
                    double pwr = PWR[j];
                    powertotal = powertotal + pwr;

                    double altitude = ALT[j];
                    alt = alt + altitude;

                    // Display data in DataGrid view
                    dataGridView1.Rows.Add(new object[] { sn++, arrHrdata[0], speed.ToString(), CAD[j], ALT[j], PWR[j] });
                    j++;
                }


            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }

        private void headerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 ff1 = new Form1();
            ff1.ShowDialog();

        }

        private void graphToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form2 fff2 = new Form2();
            fff2.ShowDialog();
        }

        private void Form3_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        string l;
        string r;

        int left;
        int right;


        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                if (e.RowIndex >= 0)
                {
                    DataGridViewRow row = this.dataGridView1.Rows[e.RowIndex];
                    l = row.Cells["SN"].Value.ToString();
                }
            }

            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                DataGridViewRow row = this.dataGridView1.Rows[e.RowIndex];
                r = row.Cells["SN"].Value.ToString();
               
            }

            left = Convert.ToInt32(l);
            right = Convert.ToInt32(r);
        }


        private void button1_Click(object sender, EventArgs e)
        {
            if (left < right)
            {
                graph obj = new graph();
                obj.leftvalue = left;
                obj.rightvalue = right;
                
                obj.ShowDialog();
            }
            else
            {
                MessageBox.Show("Sorry Invalid");
            }
        }



        private void monthCalendar1_DateSelected(object sender, DateRangeEventArgs e)
        {
         String a =  e.Start.Date.ToString();


         if ("5/1/2016 12:00:00 AM" == a)
            {
                
                calender_support obj = new calender_support();
                string file = "ASDBExampleCycleComputerData.hrm";
                obj.label9.Text = file;
                obj.ShowDialog();
            }
            

            else if("5/4/2016 12:00:00 AM" == a)
            {
                calender_support obj = new calender_support();
                string file = "Data1.hrm";
                obj.label9.Text = file;
                obj.ShowDialog();

            }

            else if ("5/7/2016 12:00:00 AM" == a)
            {
                calender_support obj = new calender_support();
                string file = "Data2.hrm";
                obj.label9.Text = file;
                obj.ShowDialog();

            }
            else
            {
                MessageBox.Show("failed");
            }

        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            
        }

        private void advanceMetricsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Advancemetric ad = new Advancemetric();
            ad.ShowDialog();
        }

        private void intervalDetectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            interval ins = new interval();
            ins.ShowDialog();
        }



         

    }
}
