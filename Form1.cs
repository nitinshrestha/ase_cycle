﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace Cycle
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            int[] HR;
            int[] SPD;
            int[] CAD;
            int[] ALT;
            int[] PWR;
            


            try
            {
                string txtData = File.ReadAllText("ASDBExampleCycleComputerData.hrm");
                string[] arrData = txtData.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                int index2 = Array.IndexOf(arrData, "[HRData]");
                HR = new int[arrData.Length - (index2 + 1)];
                SPD = new int[arrData.Length - (index2 + 1)];
                CAD = new int[arrData.Length - (index2 + 1)];
                ALT = new int[arrData.Length - (index2 + 1)];
                PWR = new int[arrData.Length - (index2 + 1)];

                int j = 0;
                double total = 0;
                double heart = 0;
                double powertotal = 0;
                double alt = 0;


                for (int i = index2 + 1; i < arrData.Length; i++)
                {
                    string HRData = arrData[i];
                    string[] arrHrdata = Regex.Split(HRData, @"\W+");

                    HR[j] = Convert.ToInt32(arrHrdata[0]);
                    SPD[j] = Convert.ToInt32(arrHrdata[1]);
                    CAD[j] = Convert.ToInt32(arrHrdata[2]);
                    ALT[j] = Convert.ToInt32(arrHrdata[3]);
                    PWR[j] = Convert.ToInt32(arrHrdata[4]);


                    // for Average speed
                    double speed = Convert.ToDouble(arrHrdata[1]) / 10;
                    double xa = SPD[j];
                    total = total + xa;

                    //Average heart Rate
                    double hrt = HR[j];
                    heart = heart + hrt;


                    //Average power
                    double pwr = PWR[j];
                    powertotal = powertotal + pwr;

                    double altitude = ALT[j];
                    alt = alt + altitude;

                   j++;
                }


                double km = total / 10;

               

                //FOr average speed
                double avgspeed = km / (j);
                this.label24.Text = avgspeed.ToString("#.####");


                //for  maxspeed
                double maxspeed = SPD.Max();
                double maxvalue = maxspeed / 10;
                this.label26.Text = maxvalue.ToString();


                //for average Heart Rate
                double avghr = heart / (j);
                this.label18.Text = avghr.ToString("#.###");

                //for maximum Hr data
                int maxhrt = HR.Max();
                this.label14.Text = maxhrt.ToString();

                //for minimum Hr data
                int minhrt = HR.Min();
                this.label16.Text = minhrt.ToString();


                //for average power
                double avgpwr = powertotal / (j);
                this.label20.Text = avgpwr.ToString("#.###");


                //for maximum power data
                int maxpwr = PWR.Max();
                this.label22.Text = maxpwr.ToString();

                //for average ALT data
                double avgalt = alt / (j);
                this.label28.Text = avgalt.ToString("#.###");

                //For Maximum ALT data
                int Alt = ALT.Max();
                this.label30.Text = Alt.ToString();


            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }



            foreach (string line in File.ReadAllLines("ASDBExampleCycleComputerData.hrm"))
            {
                string[] parts = line.Split('=');


                if (parts[0].Equals("Version"))
                {

                    label2.Text = parts[1];
                }

                if (parts[0].Equals("Date"))
                {

                    label4.Text = parts[1];
                }

                if (parts[0].Equals("Interval"))
                {

                    label6.Text = parts[1];
                }

                if (parts[0].Equals("Monitor"))
                {

                    label8.Text = parts[1];
                }

                if (parts[0].Equals("SMode"))
                {

                    label10.Text = parts[1];
                }

                if (parts[0].Equals("StartTime"))
                {

                    label12.Text = parts[1];
                }
            }

        }

        private void graphToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form2 f = new Form2();
            f.ShowDialog();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void dataGridViewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form3 ab3 = new Form3();
            ab3.ShowDialog();
        }

        private void task3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
           
        }

        private void task4ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Advancemetric ad = new Advancemetric();
            ad.ShowDialog();

        }

        private void tTask5ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            interval ins = new interval();
            ins.ShowDialog();

        }


    }


}

        
  

